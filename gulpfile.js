var gulp = require("gulp");
var pagebuilder = require('gulp-file-include');

var path = {
    src: {
        html: 'src/html/index.html',
        js: 'src/js/main.js'
    },
    build: {
        html: 'build',
        js: 'build/assets/js'
    }
}

gulp.task('inject', function () {
    var htmlInject = gulp.src(path.src.html)
        .pipe(pagebuilder({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(path.build.html));

    var jsInject = gulp.src(path.src.js)
        .pipe(pagebuilder({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('watch', ['inject'], function () {
    gulp.watch('src/**/*', ['inject']);
});
// sprite gen
var spritesmith = require('gulp.spritesmith');
var gulpif = require('gulp-if');

gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/*.{png,jpg}').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css'
    }));
    return spriteData.pipe(gulpif('*.png', gulp.dest('build/assets/img/footer'), gulp.dest('src/sass')));
});

gulp.task('default', ['watch']);