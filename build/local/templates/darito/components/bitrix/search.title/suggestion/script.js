$().ready(function () {
    var searchUrl = 'https://darito.ru/include/search_autocomplete.php';
    var $form = $('#header_search').parents('form');
    var $searchResult = $('#header_search_result');
    var daritoSearchTimeout = null;

    $(document).on('click', '#header_search_result .hide', function () {
        $searchResult.slideUp(100);
    });

    $(document).on('click', '#header_search_result .header_search-hidden-all', function (e) {
        e.preventDefault();
        $form.submit();
    });

    $(document).on('keyup', '#header_search', function (key) {
        //не загружать если
        if (key.keyCode == 38  //up
            || key.keyCode == 13  // down
            || key.keyCode == 40  // enter
        ) {
            return false;
        }

        var query = $(this).val();

        if (query.length < 3) {
            $searchResult.html('');
            $searchResult.slideUp(100);
            return false;
        }

        clearTimeout(daritoSearchTimeout);
        daritoSearchTimeout = setTimeout(function () {
            // Do AJAX shit here
            $.ajax({
                url: searchUrl,
                type: 'POST',
                data: 'ajax_call=y&l=2&q=' + query,
                tryCount: 0,
                retryLimit: 2,
                beforeSend: function () {},
                success: function (html) {
                    $searchResult.html(html);

                    if (!$searchResult.is(':visible')) {
                        $searchResult.slideDown();
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (textStatus == 'timeout') {
                        this.tryCount++;
                        if (this.tryCount <= this.retryLimit) {
                            //try again
                            console.log('try again!');
                            $.ajax(this);
                            return;
                        }
                        return;
                    }
                    if (xhr.status == 500) {
                        //handle error
                        console.log('500 xhr.status!');
                    } else {
                        //handle error
                        console.log('Other xhr.status = ' + xhr.status);
                    }
                }
            });
        }, 500);
    });

});
