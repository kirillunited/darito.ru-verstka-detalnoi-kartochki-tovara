$(document).ready(function () {
    $("body").on("click", ".header_nav-city-hidden_cross" , function () {  
        $(".header_nav-city-hidden").hide();
        $(".header_nav-city").removeClass("open");
    });
    $("body").on("click", ".header_nav-city", function () {/*08.06.18*/
        $(".header_nav-city-hidden").show();
        $(this).addClass("open");
    });

    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var city = $(".header_nav-city");
            var hiddenCity = $(".header_nav-city-hidden");
            if (!hiddenCity.is(e.target) && hiddenCity.has(e.target).length === 0 && !city.is(e.target) && city.has(e.target).length === 0) {
                $(".header_nav-city-hidden").hide();
                $(".header_nav-city").removeClass("open");
            }
        });
    });

    $("body").on("keyup", ".header_search input[type='text']", function () {
        if ($(".header_search input[type='text']").val().length < 1) {
            $(".header_search-hidden").hide();
        }
        else{
            if($('#header_search_result').html().length > 1) {
                $(".header_search-hidden").show();
            }   
        }
    });
    
    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var search = $(".header_search-hidden");
            if (!search.is(e.target) && search.has(e.target).length === 0) {
                search.hide();
            }
        });
    });

    $("body").on("click", ".header_catalog", function () {
        if ($(this).hasClass("open")) {
            $(".header_catalog-hidden").hide();
            $(this).removeClass("open");
        }
        else {
            $(".header_catalog-hidden").show();
            $(this).addClass("open");
            $(".nano").nanoScroller({});
        }
    });
    
    jQuery(function ($) {
        $(document).mouseup(function (e) {
            var catBtn = $(".header_catalog");
            var hiddenCat = $(".header_catalog-hidden");
            if (!hiddenCat.is(e.target) && hiddenCat.has(e.target).length === 0 && !catBtn.is(e.target) && catBtn.has(e.target).length === 0) {
                $(".header_catalog-hidden").hide();
                $(".header_catalog").removeClass("open");
            }
        });
    });

    $(".header_catalog-mobile-item-wrap ul li a").on("click", function () {
        if ($(this).hasClass("open")) {
            $(this).siblings(".header_catalog-mobile-item-child").slideUp('fast');
            $(this).removeClass("open").siblings(".header_catalog-mobile-item-arr").removeClass("active");
            $(this).parent("li").removeClass("open");
        }
        else {
            $(this).siblings(".header_catalog-mobile-item-child").slideDown('fast');
            $(this).addClass("open").siblings(".header_catalog-mobile-item-arr").addClass("active");
            $(this).parent("li").addClass("open");
        }
    });

    $(window).on('resize', function () {/*08.06.18*/
        var win = $(this);
        if (win.width() >= 768) {
            $(".header_search").removeAttr("style");
            $(".show-mobile-search").removeClass("active");
        }
    });

    $("body").on("click", ".header_catalog-mobile-item-cross", function () {
        $(".header_catalog-hidden").hide();
        $(".header_catalog").removeClass("open");
    });

    $("body").on("click", ".show-mobile-search", function () { 
        $(this).toggleClass("active");
        $(".header_search").toggle();
    });
    $("body").on("click", ".search-close", function () {
        $(".header_search").hide();
        $(".show-mobile-search").removeClass("active");
    });

    fixedHeader();
    $(document).scroll(function () {
        fixedHeader();
    });
    $(window).resize(function () {
        fixedHeader();
    });


    $("body").on("mouseover", ".header_catalog-withchild", function () {
        var menuId = $(this).data("menuid");
        var menuIdLast = $(this).data("menuid-last");
        if ($(this).closest(".header_catalog-item-child").length > 0) {
            $('.header_catalog-item-child').filter("[data-menuid-last]").removeClass("open");
            $(this).addClass("active").siblings("li").removeClass("active");
            $('.header_catalog-item-child').filter("[data-menuid-last='" + menuIdLast + "']").addClass("open");
        }
        else{
            $(".header_catalog-withchild").removeClass("active");
            $('.header_catalog-item-child').removeClass("open");
            $(this).addClass("active");
            $('.header_catalog-item-child').filter("[data-menuid='" + menuId + "']").addClass("open");
        }
    });
    $("body").on("mouseover", ".header_catalog-hidden li", function () {
        if ($(this).data("menuid-last") == undefined && $(this).parents(".header_catalog-item-child.open").data("menuid") > 0) {
            $("[data-menuid-last].open").removeClass("open");
            $(this).siblings("li").removeClass("active");
        }
        $(".nano").nanoScroller({});
    });
    $('body').on('click', '.header_request-call_js', function (e) {
        $.fancybox({
            type: 'inline',
            href: '.request-call-form',
            closeBtn: '',
            scrolling: 'visible',
            afterLoad: function () {
                $(".fancybox-overlay").addClass('transparent');
            },
            beforeClose: function () {
                $(".fancybox-skin").addClass('flipOutX animated');
            }
        });
    });
    $('body').on('submit','#requestCallForm', function() { 
        var form = $(this);
        if (form.find('input[name=name]').val() == '') {
            alert("Пожалуйста, укажите ваше имя");
        } else if (form.find('input[name=phone]').val() == '') {
            alert("Пожалуйста, укажите телефон для связи");
        } else if (form.find('#personal:checked').length == 0) {
            alert("Вы не дали согласие на обработку персональных данных!");
        }
        else {
            var str = form.serialize();
            $.post('https://darito.ru/ajax/send_request_call.php', str, function(data){
                response = JSON.parse(data);
                if (response.result == 'ok') {
                    form.prev().html(response.text).css('color','900');
                    form.remove();
                }
            });
        }
        return false;
    });

});


function fixedHeader() {
    if ($(document).scrollTop() + $(window).height() > $(".header").offset().top && $(document).scrollTop() - $(".header").offset().top < $(".header").height()) {
        $(".header_bottom").removeClass("fixed");
    }
    else {
        $(".header_bottom").addClass("fixed")
    }
}

